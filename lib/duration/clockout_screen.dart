import 'package:flutter/material.dart';
import 'package:poca_mobile/pages/main_page.dart';

import '../pages/home_page.dart';
import '../utils/global_color.dart';

class ClockOutScreen extends StatefulWidget {
  const ClockOutScreen({super.key});

  @override
  State<ClockOutScreen> createState() => _ClockOutScreenState();
}

class _ClockOutScreenState extends State<ClockOutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Clock Out",
          style: TextStyle(
            color: GlobalColors.clockincolor,
          ),
        ),
        automaticallyImplyLeading: false,
        actions: [
          SizedBox(
            width: 75,
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: ((context) => const MainPage())));
                    },
                    icon: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
