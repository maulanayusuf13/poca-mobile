// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/flutter_map.dart'; // Suitable for most situations
import 'package:flutter_map/plugin_api.dart'; // Only import if required functionality is not exposed by default
import 'package:latlong2/latlong.dart';
// import 'package:mapbox_gl/mapbox_gl.dart';

import '../pages/main_page.dart';
import '../utils/global_color.dart';

class ClockInScreen extends StatefulWidget {
  const ClockInScreen({super.key});

  @override
  State<ClockInScreen> createState() => _ClockInScreenState();
}

class _ClockInScreenState extends State<ClockInScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Clock In",
          style: TextStyle(
            color: GlobalColors.clockincolor,
          ),
        ),
        automaticallyImplyLeading: false,
        actions: [
          SizedBox(
            width: 75,
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: ((context) => const MainPage())));
                    },
                    icon: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ))
              ],
            ),
          )
        ],
      ),
      
      body: FlutterMap(
        options: MapOptions(
          center: LatLng(-6.6781053, 106.855064),
          zoom: 15,
        ),
        children: [
          TileLayer(
            urlTemplate:
                'https://api.mapbox.com/styles/v1/maulanayusuf/clhcr9aef00y101qtee9f0vz4/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibWF1bGFuYXl1c3VmIiwiYSI6ImNsaDdkY24zYjA3d3EzZW1wYnZhMnozeTIifQ.VbFi9L-wHRamWbR0WePB0g',
            userAgentPackageName: 'com.example.app',
          ),
          MarkerLayer(
            markers: [
              Marker(
                  point: LatLng(-6.6781053, 106.855064),
                  builder: (context) => Container(
                        child: Icon(
                          Icons.location_on,
                          color: Colors.red,
                        ),
                      ))
            ],
          )
        ],
      ),
    );
  }
}
