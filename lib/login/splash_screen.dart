import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poca_mobile/login/login_screen.dart';
import 'package:poca_mobile/utils/global_color.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Timer(const Duration(seconds: 2), () {
      Get.to(const LoginScreeen());
    });
    return Scaffold(
      backgroundColor: GlobalColors.bodycolor,
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/POCA.png",
            width: 100,
          ),
        ],
      )),
    );
  }
}
