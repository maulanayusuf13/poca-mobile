// import 'dart:html';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poca_mobile/pages/main_page.dart';
import 'package:poca_mobile/utils/global_color.dart';

class LoginScreeen extends StatefulWidget {
  const LoginScreeen({super.key});

  @override
  State<LoginScreeen> createState() => _LoginScreeenState();
}

class _LoginScreeenState extends State<LoginScreeen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  // final String longText =
  //     "Selamat datang kembali, Pocazen Silahkan isi terlebih dahulu data dari anda sebelum memulai ya!";

  // Future sigIn() async {
  //   await FirebaseAuth.instance.signInWithEmailAndPassword(
  //       email: _emailController.text,
  //       password: _passwordController.text.trim());
  // }

  // @override
  // void discope() {
  //   _emailController.dispose();
  //   _passwordController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                Colors.white,
                // Color.fromARGB(255, 237, 244, 255),
                Color.fromARGB(255, 219, 236, 250),
                Color.fromARGB(255, 190, 226, 255)
              ])),
          width: double.infinity,
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 30,
              ),
              Container(
                child: Column(
                  children: [
                    Text(
                      "V 0.9.1 (2023)",
                      style: GoogleFonts.jaldi(
                          textStyle: const TextStyle(fontSize: 15)),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 100,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Helo dari",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold, fontSize: 22)),
                    Row(
                      children: [
                        // Image.asset(
                        //   "assets/POCA.png",
                        //   width: 30,
                        // ),
                        Text(
                          "Poca Goup",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        )
                      ],
                    )
                  ],
                ),
              ),
              // const SizedBox(height: 5),
              Container(
                child: Text(
                  "Selamat datang kembali, Pocazen Silahkan isi terlebih dahulu data dari anda sebelum memulai ya",
                  style: GoogleFonts.poppins(fontSize: 15),
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              Container(
                child: TextFormField(
                  controller: _emailController,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      // borderSide: BorderSide(color: GlobalColors.inputcolor),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: GlobalColors.inputcolor, width: 1.1)),
                    hintText: "Email / Mobile (6281xxxxxxxxxxx)",
                    labelText: "Email / Mobile (6281xxxxxxxxxxx)",
                  ),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Container(
                child: TextFormField(
                  controller: _passwordController,
                  keyboardType: TextInputType.text,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      // borderSide: BorderSide(
                      //     color: GlobalColors.inputcolor, width: 2.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: GlobalColors.inputcolor, width: 1.1)),
                    hintText: "Password",
                    labelText: "Password",
                  ),
                ),
              ),
              const SizedBox(height: 40),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                    color: GlobalColors.maincolor,
                    borderRadius: BorderRadius.circular(29)),
                child: ElevatedButton(
                  onPressed: (() {
                    if (_emailController.text == "" ||
                        _passwordController.text == "") {
                      print("Masukan email dan password");
                      return;
                    }
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const MainPage()));
                    // Navigator.pushReplacement<void, void>(
                    //     context,
                    //     MaterialPageRoute<void>(
                    //         builder: ((context) => const MainPage())));
                    // if (_usernameController.text == "" ||
                    //     _passwordController.text == "") {
                    //   printError();
                    // } else {
                    //   print("Succes");
                    // }
                    print("Succes");
                  }),
                  style: ElevatedButton.styleFrom(
                      primary: GlobalColors.maincolor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  child: Text(
                    "Login",
                    style: GoogleFonts.jaldi(
                        textStyle: const TextStyle(color: Colors.white)),
                  ),
                ),
              ),
              const SizedBox(height: 95),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: "Butuh Bantuan? Hubungi kami di ",
                        style: GoogleFonts.poppins(color: Colors.black),
                      ),
                      TextSpan(
                          text: "Layanan Pelanggan",
                          style: GoogleFonts.poppins(
                              color: GlobalColors.colorLayanancolor))
                    ]),
                  ),
                ),
              )
              // Container(
              //     // child: Center(
              //     child: RichText(
              //         text: TextSpan(children: [
              //       TextSpan(
              //           text: "Butuh Bantuan? Hubungi kami di ",
              //           style: GoogleFonts.poppins(color: Colors.black)),
              //       TextSpan(
              //           text: "Layanan Pelanggan",
              //           style: GoogleFonts.poppins(
              //               color: GlobalColors.colorLayanancolor))
              //     ]))
              //     // Text("Butuh Bantuan? Hubungi kami di Layanan Pelanggan"),
              //     ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
