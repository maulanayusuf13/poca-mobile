import 'package:flutter/material.dart';
import 'package:poca_mobile/task/add.task_page.dart';

class TaskPage extends StatefulWidget {
  const TaskPage({super.key});

  @override
  State<TaskPage> createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(
                    Icons.add_task_outlined,
                  ),
                  // SizedBox(
                  //   width: 5,
                  // ),
                  Text("Task"),
                  SizedBox(
                    width: 150,
                  ),
                  Icon(
                    Icons.refresh,
                  ),
                  // Expanded(
                  //   flex: 2,
                  //   child: Icon(Icons.refresh),
                  // ),
                  IconButton(
                      onPressed: () {
                        // Navigator.pushReplacement(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => AddTaskPage()));
                      },
                      icon: Icon(Icons.add))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: const [
                  Text("In Progress"),
                  SizedBox(
                    width: 15,
                  ),
                  Icon(Icons.refresh)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
