import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:poca_mobile/pages/home_page.dart';
import 'package:poca_mobile/pages/profile_page.dart';
import 'package:poca_mobile/pages/task_page.dart';
import 'package:poca_mobile/utils/global_color.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedMenu = 0;
  List<Widget> listPage = const [
    HomePage(),
    TaskPage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Aplikasi POCA"),
      // ),
      bottomNavigationBar: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
          child: GNav(
            backgroundColor: Colors.white,
            color: GlobalColors.hurufnavcolor,
            activeColor: GlobalColors.hurufnavcolor,
            duration: const Duration(milliseconds: 300),
            tabBackgroundColor: GlobalColors.navbarcolor,
            gap: 8,
            onTabChange: (Value) {
              setState(() {
                _selectedMenu = Value;
              });
            },
            padding: const EdgeInsets.all(16),
            tabs: const [
              GButton(
                icon: Icons.home_outlined,
                text: 'Home',
              ),
              GButton(
                icon: Icons.add_task_outlined,
                text: 'Task',
              ),
              GButton(
                icon: Icons.person_outline_outlined,
                text: 'Profile',
              ),
            ],
            // onTabChange: ((value) {
            //   setState(() {
            //     _selectedMenu = value;
            //   });
            // }),
          ),
        ),
      ),
      body: listPage[_selectedMenu],
    );
  }
}
