import 'package:hexcolor/hexcolor.dart';

class GlobalColors {
  static HexColor bodycolor = HexColor('#EBEDF5');

  static HexColor maincolor = HexColor('#181762');

  static HexColor inputcolor = HexColor('#A3EA97');

  static HexColor versioncolor = HexColor('#FF0000');

  static HexColor navbarcolor = HexColor('#D0FAEB');

  static HexColor hurufnavcolor = HexColor('#087806');

  static HexColor infoversioncolor = HexColor('#FF0000');

  static HexColor clockinclockoutcolor = HexColor('#3A486C');

  static HexColor clockincolor = HexColor("#40678B");

  static HexColor colorLayanancolor = HexColor("#2F87D9");

  static HexColor cibackgorund = HexColor("#00233D");
}
